module github.com/n-insaidoo/go-sample-dependency

go 1.12

require (
	github.com/aws/aws-lambda-go v1.17.0
	github.com/aws/aws-sdk-go v1.31.5
)
